// There's an issue with ParcelJS around the sharing of assets on different pages
// These should be fixed in Parcel 2 https://github.com/parcel-bundler/parcel/issues/1931
// Little hack to require the sass file here for development purposes to enable HMR
if (process.env.NODE_ENV !== 'production') {
  require('../sass/index.scss');
}

class Test {
  test() {
    console.log('Edit me in src/js');
  }
}

let test = new Test();
test.test();
