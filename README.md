# Static site  ParcelJS/Pug

### Installation
```console
$ yarn
```

Development:
```console
$ yarn start
```

Production:
```console
$ yarn serve:prod
```